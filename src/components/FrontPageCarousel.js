import Carousel from 'react-bootstrap/Carousel';
import prodsamp1 from './images/prodsamp1.jpg';
import prodsamp2 from './images/prodsamp2.jpg';
import prodsamp3 from './images/prodsamp3.jpg';
import prodsamp4 from './images/prodsamp4.jpg';
import { Fragment } from 'react';
import './FrontPageCarousel.css'

export default function HomeCarousel() {
    return(
        
        <Fragment id="FrontPageCarousel">
            <Carousel pauseOnHover={false} controls={false} indicators={false} id="carousel" className="offset-8" interval={2000}>
                <Carousel.Item>
                    <img fluid="true" id="carouselImg1" src ={prodsamp1} />
                </Carousel.Item>
                <Carousel.Item >
                    <img fluid="true" id="carouselImg2" src ={prodsamp2} />
                </Carousel.Item>
                <Carousel.Item>
                    <img fluid="true" id="carouselImg3" src ={prodsamp3} />
                </Carousel.Item>
                <Carousel.Item>
                    <img fluid="true" id="carouselImg4" src ={prodsamp4} />
                </Carousel.Item>
            </Carousel>
        </Fragment>
        
    )
    
}