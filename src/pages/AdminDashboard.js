import { Fragment } from "react";
import { Card } from "react-bootstrap";
import ProductsContainer from "../components/ProductsContainer.js";
import UsersContainer from "../components/UsersContainer.js";
import { Row, Col } from "react-bootstrap";
import './css/AdminDashboard.css';
import {Button} from "react-bootstrap";
import {Container} from 'react-bootstrap';
import AddProduct from "../components/AddProductModal.js";

export default function AdminDashboard() {
    return(
        <Fragment>
        <div id="admindashmain">
            <div className="mt-5 mx-4">
                <Card className="mx-5 p-5" id="welcomeBannerDash">
                    <Card.Title>Welcome, {localStorage.firstName}!</Card.Title>
                    <Card.Body>You can manage your products and user permissions here.</Card.Body>
                </Card>
            </div>
            <Container fluid style={{zIndex:'5'}}>
                <Row style={{height:'80vh'}}>
                    <Col id="admincards" xs='10' md='5'
                    className="m-auto my-md-auto h-75 scrollbar"
                    style={{border:"1px solid black", boxShadow:"4px 4px 8px 0px rgba(0,0,0,0.2)", borderRadius:"10px", overflowY:'scroll', zIndex:4,overflowX:'hidden'}}>
                        
                        <div>
                            <h1>Products</h1>
                        
                            <AddProduct />
                        </div>

                        <ProductsContainer />

                    </Col>
                    <Col id="admincards" xs='10' md='5'
                    className="m-auto my-md-auto h-75 scrollbar"
                    style={{border:"1px solid black", boxShadow:"4px 4px 8px 0px rgba(0,0,0,0.2)", borderRadius:"10px", overflowY:'scroll', zIndex:4,overflowX:'hidden'}}>
                        <div><h1>Users</h1></div>
                        <UsersContainer />

                    </Col>
                </Row>
            </Container>
            </div>
        </Fragment>
    )
}